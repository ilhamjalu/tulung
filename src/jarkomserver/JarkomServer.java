/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jarkomserver;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 *
 * @author Aleksis Xancez
 */
public class JarkomServer {

    /**
     * @param args the command line arguments
     */
    static Vector<Players> ar = new Vector<>();
    static int i = 0;

    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        System.out.println("Server Start");
        ServerSocket serversocket = new ServerSocket(4444);
        Socket socket;
        while(true){
        socket = serversocket.accept();
        System.out.println(socket.getInetAddress() + "Connected");
        DataOutputStream out = new DataOutputStream(socket.getOutputStream());
        DataInputStream in = new DataInputStream(socket.getInputStream());
        Players th = new Players(socket,"Player connected" + i, in, out);
        Thread thread = new Thread(th);
        ar.add(th);
        thread.start();
        i++;
        }
        }

    }
class Players implements Runnable{
    
    Scanner scan = new Scanner(System.in);
    final DataOutputStream dos;
    final DataInputStream dis;
    private String name;
    Socket socket;
    boolean login;
    private String string;
    public Players(Socket socket, String name, DataOutputStream dos, DataInputStream dis){
        this.dos = dos;
        this.dis = dis;
        this.name = name;
        this.socket = socket;
        this.login=true;
    }

    public Players(Socket socket, String string, DataInputStream dis, DataOutputStream dos) {
        this.socket = socket;
        this. string = string;
        this.dos = dos;
        this.dis = dis;
    }
    
    public void run() {
     
        while(true){
            try {
                int[] arr = new int[10];
                String input = dis.readUTF(); 

			if(input.equals("bye"))
				break; 

			System.out.println("Input Client 1: " + input);
			int resClient_1;
 
			StringTokenizer st = new StringTokenizer(input);  
                        
			int oprnd1 = Integer.parseInt(st.nextToken()); 
			String operation = st.nextToken(); 
			int oprnd2 = Integer.parseInt(st.nextToken());

                        if(oprnd1 == oprnd2){
                            resClient_1 = 0;
                            //dos.writeUTF(Integer.toString(resClient_1)); 
                        } else{
                            if (operation.equals("+")) 
                                { 
                                    resClient_1 = oprnd1 + oprnd2;
                                } 

                            else if (operation.equals("-")) 
                                { 
                                    resClient_1 = oprnd1 - oprnd2; 
                                } 
                            else if (operation.equals("*")) 
                                { 
                                    resClient_1 = oprnd1 * oprnd2;
                                } 
                            else
                                { 
                                    resClient_1 = oprnd1 / oprnd2;
                                } 
                            
                                //for(int a=0; a<10; a++){                        
                                  // arr[a] = resClient_1;
                                //}
                        
                                int temp, i;
                                for(i = 1; i < arr.length; i++){
                                    for(int j = i; j > 0; j--){
                                        if(arr[j] < arr[j-1]){
                                            temp = arr[j];
                                            arr[j] = arr[j-1];
                                            arr[j-1] = temp;
                                        }
                                    }
                                }
                            
                        }
                        
                        System.out.println("Mengirim Hasil"); 
                       // for (int i = 0; i < arr.length; i++) {
                             dos.writeUTF(Integer.toString(resClient_1));
                        //} 
                        
                        
                        
            } catch (IOException e) {
               e.printStackTrace();
            }
        }
        try {
        this.dis.close();
        this.dos.close();
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}

